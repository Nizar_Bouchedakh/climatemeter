package com.example.asus_pc.climatmeter;

/**
 * Created by ASUS-PC on 30/04/2017.
 */

public class MyThread_GPS extends Thread implements Runnable {

    private Object mPauseLock;
    private boolean mPaused;
    private boolean mFinished;

    MapFragment activity;
    int N;
    int F;
    String IP = "192.168.2.1";

    public MyThread_GPS(MapFragment activity, int N, int F, String IP){
        this.activity=activity;
        this.N=N;
        this.F=F;
        this.IP=IP;
        mPauseLock = new Object();
        mPaused = false;
        mFinished = false;
    }

    public void run() {
        while (!mFinished) {

            synchronized (mPauseLock) {
                try {
                    new Sender_GPS(this.activity).execute("http://"+this.IP+":5000/get_GPS_path/"+Integer.toString(this.N));
                    mPauseLock.wait(this.F*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                while (mPaused) {
                    try {
                        mPauseLock.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    /**
     * Call this on pause.
     */
    public void onPause() {
        synchronized (mPauseLock) {
            mPaused = true;
        }
    }

    /**
     * Call this on resume.
     */
    public void onResume() {
        synchronized (mPauseLock) {
            mPaused = false;
            mPauseLock.notifyAll();
        }
    }



}
