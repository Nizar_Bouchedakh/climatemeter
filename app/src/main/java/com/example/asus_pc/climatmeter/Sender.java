package com.example.asus_pc.climatmeter;

import android.os.AsyncTask;
import android.widget.ImageButton;

import com.jjoe64.graphview.series.DataPoint;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * Created by ASUS-PC on 28/04/2017.
 */

public class Sender extends AsyncTask<String, Void, String[]> {

    private Fragment1 activity1;
//    private Fragment3 activity2;

    Sender(Fragment1 activity1){
        super();
        this.activity1 = activity1;
//        // this.activity2 = activity2;
    }

    private Exception exception;

    protected String[] doInBackground(String... urls) {
        String r;
        String[] tab = new String[2] ;
        try {
            r = requestContent(urls[0]);
            // Log.i("Test", r);
            tab[0]=r;


            r = requestContent(urls[1]);
            // Log.i("Test", r);
            tab[1]=r;
        }
        catch(Exception e){
            r= e.toString();
            // Log.i("Test0",r);
        }
        return tab;
    }

    // to send request
    public String requestContent(String url) {
        HttpClient httpclient = new DefaultHttpClient();
        String result = null;
        HttpGet httpget = new HttpGet(url);
        HttpResponse response = null;
        InputStream instream = null;

        try {
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                instream = entity.getContent();
                result = convertStreamToString(instream);
            }

        } catch (Exception e) {
            // manage exceptions
            // Log.i("Test1", e.toString());
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (Exception exc) {
                    // Log.i("Test2", exc.toString());
                }
            }
        }

        return result;
    }

    // convert data stream into string
    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            // Log.i("Test3", e.toString());
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                // Log.i("Test4", e.toString());
            }
        }

        return sb.toString();
    }

    protected void onPostExecute(String[] tab) {
        // TODO: check this.exception
        // TODO: do something with the feed
        // TODO: probably create timeseries and update graphs
try {
    String r = tab[0];

    String[] measures = r.split(";");
    // Log.i("Test5", measures[0]);
    DataPoint[] tempdata = new DataPoint[measures.length - 1];
    DataPoint[] noisedata = new DataPoint[measures.length - 1];
    DataPoint[] Hdata = new DataPoint[measures.length - 1];
    DataPoint[] Solardata = new DataPoint[measures.length - 1];

    DataPoint[] TCBdata = new DataPoint[measures.length - 1];
    DataPoint[] TCMdata = new DataPoint[measures.length - 1];
    DataPoint[] TCTdata = new DataPoint[measures.length - 1];

    DataPoint[] IMU_Ydata = new DataPoint[measures.length - 1];
    DataPoint[] IMU_Pdata = new DataPoint[measures.length - 1];
    DataPoint[] IMU_Rdata = new DataPoint[measures.length - 1];

    DataPoint[] IRBKdata = new DataPoint[measures.length - 1];
    DataPoint[] IRLdata = new DataPoint[measures.length - 1];
    DataPoint[] IRFdata = new DataPoint[measures.length - 1];
    DataPoint[] IRRdata = new DataPoint[measures.length - 1];
    DataPoint[] IRTdata = new DataPoint[measures.length - 1];
    DataPoint[] IRBTdata = new DataPoint[measures.length - 1];


    Double Alt = null;
    Double NB_Sat = null;
    Double AWA = null;
    Double AWS = null;
    Double TWS = null;
    Double TWD = null;
    Double Flag = null;
    Double SOG = null;
    Double COG = null;
    Double GPS_Ready = null;
    Double temp = null;
    Double noise = null;
    Double hum = null;
    Double solar = null;

    Double tcb = null;
    Double tcm = null;
    Double tct = null;

    Double imuy = null;
    Double imup = null;
    Double imur = null;

    Double irbk = null;
    Double irl = null;
    Double irf = null;
    Double irr = null;
    Double irt = null;
    Double irbt = null;


    int i = 0;
    for (int j = 2; j < measures.length - 1; j++) {

        String[] serie = measures[j].split(",");

        if ((serie.length == 29) && (!Arrays.asList(serie).contains("NAN") && (!Arrays.asList(serie).contains("Air_Temp")))) {

            String time = serie[0].replace("*#", "").replace("[", "").replace("'", "").replace("\"", "");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            Date date = null;
            try {
                date = sdf.parse(time);

            } catch (ParseException e) {
                e.printStackTrace();
            }


            if (j == measures.length - 2) {
                Alt = Double.parseDouble(serie[3].replace("'", ""));
                NB_Sat = Double.parseDouble(serie[4].replace("'", ""));
                SOG = Double.parseDouble(serie[5].replace("'", ""));
                COG = Double.parseDouble(serie[6].replace("'", ""));
                AWA = Double.parseDouble(serie[23].replace("'", ""));
                AWS = Double.parseDouble(serie[24].replace("'", ""));
                TWS = Double.parseDouble(serie[25].replace("'", ""));
                TWD = Double.parseDouble(serie[26].replace("'", ""));
                GPS_Ready = Double.parseDouble(serie[27].replace("'", ""));
                Flag = Double.parseDouble(serie[28].replace("'", "").replace("]", ""));
            }


            temp = Double.parseDouble(serie[10].replace("'", ""));
            noise = Double.parseDouble(serie[22].replace("'", ""));
            hum = Double.parseDouble(serie[11].replace("'", ""));
            solar = Double.parseDouble(serie[21].replace("'", ""));

            tcb = Double.parseDouble(serie[12].replace("'", ""));
            tcm = Double.parseDouble(serie[13].replace("'", ""));
            tct = Double.parseDouble(serie[14].replace("'", ""));

            imuy = Double.parseDouble(serie[7].replace("'", ""));
            imup = Double.parseDouble(serie[8].replace("'", ""));
            imur = Double.parseDouble(serie[9].replace("'", ""));

            irbk = Double.parseDouble(serie[15].replace("'", ""));
            irl = Double.parseDouble(serie[16].replace("'", ""));
            irf = Double.parseDouble(serie[17].replace("'", ""));
            irr = Double.parseDouble(serie[18].replace("'", ""));
            irt = Double.parseDouble(serie[19].replace("'", ""));
            irbt = Double.parseDouble(serie[20].replace("'", ""));

            // Log.i("Test7", date + " caca " + temp.toString());

            tempdata[i] = new DataPoint(date, temp);
            // Log.i("TEST22", sdf.format(tempdata[i].getX()) + " " + tempdata[i].getY() + " " + i);
            noisedata[i] = new DataPoint(date, noise);
            Hdata[i] = new DataPoint(date, hum);
            Solardata[i] = new DataPoint(date, solar);

            TCBdata[i] = new DataPoint(date, tcb);
            TCMdata[i] = new DataPoint(date, tcm);
            TCTdata[i] = new DataPoint(date, tct);

            IMU_Ydata[i] = new DataPoint(date, imuy);
            IMU_Pdata[i] = new DataPoint(date, imup);
            IMU_Rdata[i] = new DataPoint(date, imur);


            IRBKdata[i] = new DataPoint(date, irbk);
            IRLdata[i] = new DataPoint(date, irl);
            IRFdata[i] = new DataPoint(date, irf);
            IRRdata[i] = new DataPoint(date, irr);
            IRTdata[i] = new DataPoint(date, irt);
            IRBTdata[i] = new DataPoint(date, irbt);

            i++;
        }
    }


    //TODO: Use appenddata and keep viewport on current zoom position (current MinX & MaxX)
    tempdata = RemoveNull(tempdata);
    noisedata = RemoveNull(noisedata);
    Hdata = RemoveNull(Hdata);
    Solardata = RemoveNull(Solardata);

    TCBdata = RemoveNull(TCBdata);
    TCMdata = RemoveNull(TCMdata);
    TCTdata = RemoveNull(TCTdata);

    IMU_Ydata = RemoveNull(IMU_Ydata);
    IMU_Pdata = RemoveNull(IMU_Pdata);
    IMU_Rdata = RemoveNull(IMU_Rdata);

    IRBKdata = RemoveNull(IRBKdata);
    IRLdata = RemoveNull(IRLdata);
    IRFdata = RemoveNull(IRFdata);
    IRRdata = RemoveNull(IRRdata);
    IRTdata = RemoveNull(IRTdata);
    IRBTdata = RemoveNull(IRBTdata);

    // reset interfaces

    if (notNull(GPS_Ready)) {
        if (GPS_Ready == 10) {
            this.activity1.GPS_Ready.setChecked(true);
            // this.activity2.GPS_Ready.setText("GPS Ready");
        } else {
            this.activity1.GPS_Ready.setChecked(false);
            // this.activity2.GPS_Ready.setText("GPS Not Ready");
        }
    }

    if (notNull(Alt)){
        this.activity1.Alt.setText("ALT\n" + Alt.intValue()+" m") ;
        // this.activity2.Alt.setText("ALT\n" + Alt.toString());
    }

    if (notNull(NB_Sat)) {
        this.activity1.NB_Sat.setText("SATs Number\n" + NB_Sat.intValue());
        // this.activity2.NB_Sat.setText("SATs Number\n" + NB_Sat.toString());
    }

    if (notNull(Flag)) {
        this.activity1.Flag.setText("Flag\n" + Flag.intValue());
        // this.activity2.Flag.setText("Flag\n" + Flag.toString());
    }

    if (notNull(AWA)) {
        this.activity1.AWA.setText("AWA\n" + AWA.toString()+"°");
        // this.activity2.AWA.setText("AWA\n" + AWA.toString());
    }

    if (notNull(AWS)) {
        this.activity1.AWS.setText("AWS\n" + AWS.toString()+" m/s");
        // this.activity2.AWS.setText("AWS\n" + AWS.toString());
    }

    if (notNull(TWS)) {
        this.activity1.TWS.setText("TWS\n" + TWS.toString()+" m/s");
        // this.activity2.TWS.setText("TWS\n" + TWS.toString());
    }

    if (notNull(TWD)) {
        this.activity1.TWD.setText("TWD\n" + TWD.toString()+"°");
        // this.activity2.TWD.setText("TWD\n" + TWD.toString());
    }

    if (notNull(SOG)) {
        this.activity1.SOG.setText("SOG\n" + SOG.toString()+" m/s");
        // this.activity2.SOG.setText("SOG\n" + SOG.toString());
    }

    if (notNull(COG)) {
        this.activity1.COG.setText("COG\n" + COG.toString()+"°");
        // this.activity2.COG.setText("COG\n" + COG.toString());
    }
// monitor
/*
    if (notNull(temp)) {
        // this.activity2.Temp.setText("Temperature\n" + temp.toString());
    }

    if (notNull(hum)) {
        // this.activity2.Hum.setText("humidity\n" + hum.toString());
    }

    if (notNull(noise)) {
        // this.activity2.Noise.setText("Noise\n" + noise.toString());
    }

    if (notNull(solar)) {
        // this.activity2.Solar.setText("Solar\n" + solar.toString());
    }

    if (notNull(imuy)) {
        // this.activity2.IMUY.setText("IMU_Y\n" + imuy.toString());
    }

    if (notNull(imup)) {
        // this.activity2.IMUP.setText("IMU_P\n" + imup.toString());
    }

    if (notNull(imur)) {
        // this.activity2.IMUR.setText("IMU_R\n" + imur.toString());
    }

    if (notNull(tcb)) {
        // this.activity2.TCB.setText("TCB\n" + tcb.toString());
    }

    if (notNull(tcm)) {
        // this.activity2.TCM.setText("TCM\n" + tcm.toString());
    }

    if (notNull(tct)) {
        // this.activity2.TCT.setText("TCT\n" + tct.toString());
    }

    if (notNull(irbk)) {
        // this.activity2.IRBK.setText("IRBK\n" + irbk.toString());
    }

    if (notNull(irl)) {
        // this.activity2.IRL.setText("IRL\n" + irl.toString());
    }

    if (notNull(irf)) {
        // this.activity2.IRF.setText("IRF\n" + irf.toString());
    }

    if (notNull(irr)) {
        // this.activity2.IRR.setText("IRR\n" + irr.toString());
    }

    if (notNull(irt)) {
        // this.activity2.IRT.setText("IRT\n" + irt.toString());
    }

    if (notNull(irbt)) {
        // this.activity2.IRBT.setText("IRBT\n" + irbt.toString());
    }
    */



    this.activity1.Tempseries.resetData(tempdata);
    this.activity1.Noiseseries.resetData(noisedata);
    this.activity1.Humseries.resetData(Hdata);
    this.activity1.Solarseries.resetData(Solardata);

    this.activity1.TCBseries.resetData(TCBdata);
    this.activity1.TCMseries.resetData(TCMdata);
    this.activity1.TCTseries.resetData(TCTdata);

    this.activity1.IMUYseries.resetData(IMU_Ydata);
    this.activity1.IMUPseries.resetData(IMU_Pdata);
    this.activity1.IMURseries.resetData(IMU_Rdata);

    this.activity1.IRBKseries.resetData(IRBKdata);
    this.activity1.IRLseries.resetData(IRLdata);
    this.activity1.IRFseries.resetData(IRFdata);
    this.activity1.IRRseries.resetData(IRRdata);
    //this.activity1.IRTseries.resetData(IRTdata);
    this.activity1.IRBTseries.resetData(IRBTdata);


//Graph1
// set manual x bounds to have nice steps
    //this.activity1.graph.getViewport().setXAxisBoundsStatus(Viewport.AxisBoundsStatus.FIX);
    if (tempdata.length != 0) {
        this.activity1.graph1.getViewport().setMinX(tempdata[0].getX());
        this.activity1.graph1.getViewport().setMaxX(tempdata[tempdata.length - 1].getX());
        this.activity1.graph1.getViewport().setXAxisBoundsManual(true);
        //this.activity1.graph.getViewport().setScrollable(true);


// Graph2
        //this.activity1.graph.getViewport().setXAxisBoundsStatus(Viewport.AxisBoundsStatus.FIX);
        this.activity1.graph2.getViewport().setMinX(tempdata[0].getX());
        this.activity1.graph2.getViewport().setMaxX(tempdata[tempdata.length - 1].getX());
        this.activity1.graph2.getViewport().setXAxisBoundsManual(true);
        //this.activity1.graph.getViewport().setScrollable(true);


// Graph3
        //this.activity1.graph.getViewport().setXAxisBoundsStatus(Viewport.AxisBoundsStatus.FIX);
        this.activity1.graph3.getViewport().setMinX(tempdata[0].getX());
        this.activity1.graph3.getViewport().setMaxX(tempdata[tempdata.length - 1].getX());
        this.activity1.graph3.getViewport().setXAxisBoundsManual(true);
        //this.activity1.graph.getViewport().setScrollable(true);


    }

// as we use dates as labels, the human rounding to nice readable numbers
// is not necessary
    //this.activity1.graph.getGridLabelRenderer().setHumanRounding(false);


    String q = tab[1];

    String[] timers = q.split(",");
    String start = timers[0].replace("*#", "").replace("[", "").replace("\"", "");
    String current = timers[1].replace("*#", "").replace("]", "").replace("\"", "");

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
    Date date1 = null;
    Date date2 = null;

    try {
        date1 = sdf.parse(start);
        date2 = sdf.parse(current);

    } catch (ParseException e) {
        e.printStackTrace();
    }

    if ((date1 != null) && (date2 != null)) {

        this.activity1.StartTime.setText("Start Time\n" + sdf2.format(date1));
        // this.activity2.StartTime.setText("Start Time\n" + sdf2.format(date1));

        this.activity1.CurrentTime.setText("Current Time\n" + sdf2.format(date2));
        // this.activity2.CurrentTime.setText("Current Time\n" + sdf2.format(date2));

        long diff = date2.getTime() - date1.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);

        this.activity1.Duration.setText("Duration\n" + new Time((int) diffHours, (int) diffMinutes, (int) diffSeconds).toString());
        // this.activity2.Duration.setText("Duration\n" + new Time((int) diffHours, (int) diffMinutes, (int) diffSeconds).toString());

    }
    }catch(Exception e){
    // Log.i("TestException",e.toString());

    }
    }




    public DataPoint[] RemoveNull(DataPoint[] dp) {


        List<DataPoint> list = new ArrayList<DataPoint>();

        for(DataPoint s : dp) {
            if(s != null ) {
                list.add(s);
            }
        }

        dp = list.toArray(new DataPoint[list.size()]);

        return dp;
    }

    boolean notNull(Object item) {
        return item != null;
    }


    void setimage(int min1, int min2, int max1, int max2, Double val, ImageButton img){
        if((val<min1)||(val>max1)){
            img.setImageResource(R.mipmap.cercle_rouge_icone_128);
        }
        else{
            if((val<min2)||(val>max2)){
                img.setImageResource(R.mipmap.cercle_orange_icone_128);
            }
            else{img.setImageResource(R.mipmap.cercle_vert_icone_128);}
        }

    }


}
