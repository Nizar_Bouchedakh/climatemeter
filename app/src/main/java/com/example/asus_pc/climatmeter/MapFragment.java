package com.example.asus_pc.climatmeter;


import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ASUS-PC on 13/05/2017.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private View rootView;
    public static GoogleMap mMap;
    MapView mMapView;
    double lat=46.209100;
    double lng=6.136996;
    private MylocationListener locationListener = new MylocationListener();
    private LocationManager locationManager;
    MyThread_GPS T;
    int N ;
    int F =60;
    String IP;
    boolean gps_bool;


    public static MapFragment newInstance(LocationManager l, int N, int F, String IP, boolean gps_bool) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        //TODO: set args here or remove setArguments and pervious statmenet
        fragment.setArguments(args);
        fragment.locationManager=l;
        fragment.N=N;
        fragment.F=F;
        fragment.IP=IP;
        fragment.gps_bool=gps_bool;
        return fragment;
    }

    public MapFragment() {}




    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.mapfragment, container, false);
            MapsInitializer.initialize(this.getActivity());
            mMapView = (MapView) rootView.findViewById(R.id.map);
            mMapView.onCreate(savedInstanceState);
            mMapView.getMapAsync(this);
            Log.i("MAP", "create view");
        } catch (Exception e) {
            Log.e("MAP", "Inflate exception");
        }

        this.T= new MyThread_GPS(this, this.N, this.F, this.IP);
        if(gps_bool) {
            this.T.start();
        }

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        if(gps_bool) {
            this.T.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mMapView!=null) {
            //mMapView.onDestroy();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mMapView!=null) {
            mMapView.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
        Log.i("MAP", "low memory");
    }

    @Override
    public void onResume() {
        super.onResume();
        try{
            mMapView.onResume();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        if(gps_bool) {
            this.T.onResume();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this.getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1340);
        }
        else {
            mMap.setMyLocationEnabled(true);
            if(locationManager!=null) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locationListener);
            }
            lat = locationListener.getLatitude();
            lng = locationListener.getLongitude();
            LatLng pos = new LatLng(lat, lng);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(pos));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo((float) 14.0);
            mMap.animateCamera(zoom);
        }

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        // focus on current position


            //mMap.setOnMarkerClickListener(MapFragment.this);
            //mMap.setOnInfoWindowClickListener(MapFragment.this);


    }

    public void zooming(double lat, double lng) {
        LatLng pos = new LatLng(lat, lng);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pos));
        //CameraUpdate zoom = CameraUpdateFactory.zoomTo((float) 14.0);
        //mMap.animateCamera(zoom);
    }
}