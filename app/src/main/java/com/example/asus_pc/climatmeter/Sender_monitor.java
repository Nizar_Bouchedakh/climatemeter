package com.example.asus_pc.climatmeter;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.ImageButton;

import com.jjoe64.graphview.series.DataPoint;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * Created by ASUS-PC on 28/04/2017.
 */

public class Sender_monitor extends AsyncTask<String, Void, String[]> {

    private Fragment3 activity2;
    private boolean checkR = false;
    private  boolean checkO =false;
    private  boolean checkG = false;
    private  boolean isFirstRun = true;



    Sender_monitor(Fragment3 activity2, boolean isFirstRun){
        super();
        this.activity2 = activity2;
        this.isFirstRun = isFirstRun;
    }

    private Exception exception;

    protected String[] doInBackground(String... urls) {
        String r;
        String[] tab = new String[2] ;
        try {
            r = requestContent(urls[0]);
            // Log.i("Test", r);
            tab[0]=r;


            r = requestContent(urls[1]);
            // Log.i("Test", r);
            tab[1]=r;
        }
        catch(Exception e){
            r= e.toString();
            // Log.i("Test0",r);
        }
        return tab;
    }

    // to send request
    public String requestContent(String url) {
        HttpClient httpclient = new DefaultHttpClient();
        String result = null;
        HttpGet httpget = new HttpGet(url);
        HttpResponse response = null;
        InputStream instream = null;

        try {
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                instream = entity.getContent();
                result = convertStreamToString(instream);
            }

        } catch (Exception e) {
            // manage exceptions
            // Log.i("Test1", e.toString());
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (Exception exc) {
                    // Log.i("Test2", exc.toString());
                }
            }
        }

        return result;
    }

    // convert data stream into string
    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            // Log.i("Test3", e.toString());
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                // Log.i("Test4", e.toString());
            }
        }

        return sb.toString();
    }

    protected void onPostExecute(String[] tab) {
        // TODO: check this.exception
        // TODO: do something with the feed
        // TODO: probably create timeseries and update graphs
try {
    String r = tab[0];

    String[] measures = r.split(";");
    // Log.i("Test5", measures[0]);
    DataPoint[] tempdata = new DataPoint[measures.length - 1];
    DataPoint[] noisedata = new DataPoint[measures.length - 1];
    DataPoint[] Hdata = new DataPoint[measures.length - 1];
    DataPoint[] Solardata = new DataPoint[measures.length - 1];

    DataPoint[] TCBdata = new DataPoint[measures.length - 1];
    DataPoint[] TCMdata = new DataPoint[measures.length - 1];
    DataPoint[] TCTdata = new DataPoint[measures.length - 1];

    DataPoint[] IMU_Ydata = new DataPoint[measures.length - 1];
    DataPoint[] IMU_Pdata = new DataPoint[measures.length - 1];
    DataPoint[] IMU_Rdata = new DataPoint[measures.length - 1];

    DataPoint[] IRBKdata = new DataPoint[measures.length - 1];
    DataPoint[] IRLdata = new DataPoint[measures.length - 1];
    DataPoint[] IRFdata = new DataPoint[measures.length - 1];
    DataPoint[] IRRdata = new DataPoint[measures.length - 1];
    DataPoint[] IRTdata = new DataPoint[measures.length - 1];
    DataPoint[] IRBTdata = new DataPoint[measures.length - 1];


    Double Alt = null;
    Double NB_Sat = null;
    Double AWA = null;
    Double AWS = null;
    Double TWS = null;
    Double TWD = null;
    Double Flag = null;
    Double SOG = null;
    Double COG = null;
    Double GPS_Ready = null;
    Double temp = null;
    Double noise = null;
    Double hum = null;
    Double solar = null;

    Double tcb = null;
    Double tcm = null;
    Double tct = null;

    Double imuy = null;
    Double imup = null;
    Double imur = null;

    Double irbk = null;
    Double irl = null;
    Double irf = null;
    Double irr = null;
    Double irt = null;
    Double irbt = null;


    int i = 0;
    for (int j = 2; j < measures.length - 1; j++) {

        String[] serie = measures[j].split(",");

        if ((serie.length == 29) && (!Arrays.asList(serie).contains("NAN") && (!Arrays.asList(serie).contains("Air_Temp")))) {

            String time = serie[0].replace("*#", "").replace("[", "").replace("'", "").replace("\"", "");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            Date date = null;
            try {
                date = sdf.parse(time);

            } catch (ParseException e) {
                e.printStackTrace();
            }


            if (j == measures.length - 2) {
                Alt = Double.parseDouble(serie[3].replace("'", ""));
                NB_Sat = Double.parseDouble(serie[4].replace("'", ""));
                SOG = Double.parseDouble(serie[5].replace("'", ""));
                COG = Double.parseDouble(serie[6].replace("'", ""));
                AWA = Double.parseDouble(serie[23].replace("'", ""));
                AWS = Double.parseDouble(serie[24].replace("'", ""));
                TWS = Double.parseDouble(serie[25].replace("'", ""));
                TWD = Double.parseDouble(serie[26].replace("'", ""));
                GPS_Ready = Double.parseDouble(serie[27].replace("'", ""));
                Flag = Double.parseDouble(serie[28].replace("'", "").replace("]", ""));
            }


            temp = Double.parseDouble(serie[10].replace("'", ""));
            noise = Double.parseDouble(serie[22].replace("'", ""));
            hum = Double.parseDouble(serie[11].replace("'", ""));
            solar = Double.parseDouble(serie[21].replace("'", ""));

            tcb = Double.parseDouble(serie[12].replace("'", ""));
            tcm = Double.parseDouble(serie[13].replace("'", ""));
            tct = Double.parseDouble(serie[14].replace("'", ""));

            imuy = Double.parseDouble(serie[7].replace("'", ""));
            imup = Double.parseDouble(serie[8].replace("'", ""));
            imur = Double.parseDouble(serie[9].replace("'", ""));

            irbk = Double.parseDouble(serie[15].replace("'", ""));
            irl = Double.parseDouble(serie[16].replace("'", ""));
            irf = Double.parseDouble(serie[17].replace("'", ""));
            irr = Double.parseDouble(serie[18].replace("'", ""));
            irt = Double.parseDouble(serie[19].replace("'", ""));
            irbt = Double.parseDouble(serie[20].replace("'", ""));

            // Log.i("Test7", date + " caca " + temp.toString());

            tempdata[i] = new DataPoint(date, temp);
            // Log.i("TEST22", sdf.format(tempdata[i].getX()) + " " + tempdata[i].getY() + " " + i);
            noisedata[i] = new DataPoint(date, noise);
            Hdata[i] = new DataPoint(date, hum);
            Solardata[i] = new DataPoint(date, solar);

            TCBdata[i] = new DataPoint(date, tcb);
            TCMdata[i] = new DataPoint(date, tcm);
            TCTdata[i] = new DataPoint(date, tct);

            IMU_Ydata[i] = new DataPoint(date, imuy);
            IMU_Pdata[i] = new DataPoint(date, imup);
            IMU_Rdata[i] = new DataPoint(date, imur);


            IRBKdata[i] = new DataPoint(date, irbk);
            IRLdata[i] = new DataPoint(date, irl);
            IRFdata[i] = new DataPoint(date, irf);
            IRRdata[i] = new DataPoint(date, irr);
            IRTdata[i] = new DataPoint(date, irt);
            IRBTdata[i] = new DataPoint(date, irbt);

            i++;
        }
    }


    //TODO: Use appenddata and keep viewport on current zoom position (current MinX & MaxX)
    tempdata = RemoveNull(tempdata);


    // reset interfaces

    if (notNull(GPS_Ready)) {
        if (GPS_Ready == 10) {
            this.activity2.GPS_Ready.setText("GPS Ready");
            setimage(9,11,9,11,GPS_Ready,this.activity2.GPS_ReadyImage);
        } else {
            this.activity2.GPS_Ready.setText("GPS Not Ready");
            setimage(9,11,9,11,GPS_Ready,this.activity2.GPS_ReadyImage);
        }
    }

    if (notNull(Alt)){
        this.activity2.Alt.setText("ALT\n" + Alt.intValue()+" m");
        setimage(-500,5000,-500,5000,Alt,this.activity2.AltImage);
    }

    if (notNull(NB_Sat)) {
        this.activity2.NB_Sat.setText("SATs Number\n" + NB_Sat.intValue());
        setimage(3,10,6,10,NB_Sat,this.activity2.NB_SatImage);
    }

    if (notNull(Flag)) {
        this.activity2.Flag.setText("Flag\n" + Flag.intValue());
        setimage(0,64,0,64,Flag,this.activity2.FlagImage);
    }

    if (notNull(AWA)) {
        this.activity2.AWA.setText("AWA\n" + AWA.toString()+"°");
        setimage(0,360,0,360,AWA,this.activity2.AWAImage);
    }

    if (notNull(AWS)) {
        this.activity2.AWS.setText("AWS\n" + AWS.toString()+" m/s");
        setimage(0,100,0,20,AWS,this.activity2.AWSImage);
    }

    if (notNull(TWS)) {

        this.activity2.TWS.setText("TWS\n" + TWS.toString()+" m/s");
        setimage(0,100,0,20,TWS,this.activity2.TWSImage);
    }

    if (notNull(TWD)) {
        this.activity2.TWD.setText("TWD\n" + TWD.toString()+"°");
        setimage(0,360,0,360,TWD,this.activity2.TWDImage);
    }

    if (notNull(SOG)) {
        this.activity2.SOG.setText("SOG\n" + SOG.toString()+" m/s");
        setimage(0,120,0,120,SOG,this.activity2.SOGImage);
    }

    if (notNull(COG)) {
        this.activity2.COG.setText("COG\n" + COG.toString()+"°");
        setimage(0,360,0,360,COG,this.activity2.COGImage);
    }

    if (notNull(temp)) {
        this.activity2.Temp.setText("Air_Temp\n" + temp.toString()+"°C");
        setimage(-40,100,5,40,temp,this.activity2.TempImage);
    }

    if (notNull(hum)) {
        this.activity2.Hum.setText("Rhumidity\n" + hum.toString()+"%");
        setimage(0,100,10,95,hum,this.activity2.HumImage);
    }

    if (notNull(noise)) {
        this.activity2.Noise.setText("NoiseLevel\n" + noise.toString()+" dB");
        setimage(0,140,20,100,noise,this.activity2.NoiseImage);
    }

    if (notNull(solar)) {
        this.activity2.Solar.setText("Solar_gh\n" + solar.toString()+" W/m²");
        setimage(0,2000,0,1200,solar,this.activity2.SolarImage);
    }

    if (notNull(imuy)) {
        this.activity2.IMUY.setText("IMU_Y\n" + imuy.toString()+"°");
        setimage(0,360,0,360,imuy,this.activity2.IMUYImage);
    }

    if (notNull(imup)) {
        this.activity2.IMUP.setText("IMU_P\n" + imup.toString()+"°");
        setimage(-180,180,-30,30,imup,this.activity2.IMUPImage);
    }

    if (notNull(imur)) {
        this.activity2.IMUR.setText("IMU_R\n" + imur.toString()+"°");
        setimage(-180,180,-30,30,imur,this.activity2.IMURImage);
    }

    if (notNull(tcb)) {
        this.activity2.TCB.setText("T_bottom\n" + tcb.toString()+"°C");
        setimage(-40,100,5,40,tcb,this.activity2.TCBImage);
    }

    if (notNull(tcm)) {
        this.activity2.TCM.setText("T_middle\n" + tcm.toString()+"°C");
        setimage(-40,100,5,40,tcm,this.activity2.TCMImage);
    }

    if (notNull(tct)) {
        this.activity2.TCT.setText("T_top\n" + tct.toString()+"°C");
        setimage(-40,100,5,40,tct,this.activity2.TCTImage);
    }

    if (notNull(irbk)) {
        this.activity2.IRBK.setText("IR_back\n" + irbk.toString()+"°C");
        setimage(-40,100,0,80,irbk,this.activity2.IRBKImage);
    }

    if (notNull(irl)) {
        this.activity2.IRL.setText("IR_left\n" + irl.toString()+"°C");
        setimage(-40,100,0,80,irl,this.activity2.IRLImage);
    }

    if (notNull(irf)) {
        this.activity2.IRF.setText("IR_front\n" + irf.toString()+"°C");
        setimage(-40,100,0,80,irf,this.activity2.IRFImage);
    }

    if (notNull(irr)) {
        this.activity2.IRR.setText("IR_right\n" + irr.toString()+"°C");
        setimage(-40,100,0,80,irr,this.activity2.IRRImage);
    }

    if (notNull(irt)) {
        this.activity2.IRT.setText("IR_top\n" + irt.toString()+"°C");
        setimage(-40,100,0,80,irt,this.activity2.IRTImage);
    }

    if (notNull(irbt)) {
        this.activity2.IRBT.setText("IR_bottom\n" + irbt.toString()+"°C");
        setimage(-40,100,0,80,irbt,this.activity2.IRBTImage);
    }









    String q = tab[1];

    String[] timers = q.split(",");
    String start = timers[0].replace("*#", "").replace("[", "").replace("\"", "");
    String current = timers[1].replace("*#", "").replace("]", "").replace("\"", "");

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
    Date date1 = null;
    Date date2 = null;

    try {
        date1 = sdf.parse(start);
        date2 = sdf.parse(current);

    } catch (ParseException e) {
        e.printStackTrace();
    }

    if ((date1 != null) && (date2 != null)) {

        this.activity2.StartTime.setText("Start Time\n" + sdf2.format(date1));
        setimage(0,2,0,2,1.0,this.activity2.StartTimeImage);
        setimage(0,2,0,2,1.0,this.activity2.CurrentTimeImage);

        this.activity2.CurrentTime.setText("Current Time\n" + sdf2.format(date2));

        long diff = date2.getTime() - date1.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);


        this.activity2.Duration.setText("Duration\n" + new Time((int) diffHours, (int) diffMinutes, (int) diffSeconds).toString());


        setimage(0,2,0,1,(double)diffHours,this.activity2.DurationImage);



        // message

        if((isFirstRun)&&(this.activity2.getUserVisibleHint())){
        String m = null;
        if(checkR){
        m = " You have RED Alert(s)\n\n The mission is NO GO !";
            buildAlertMessage(m);
        }else{
            if(checkO){
                m = " You have YELLOW Alert(s)\n\n The mission is a GO : Expert Mode\n" +
                        "\n" +
                        " Please check :\n" +
                        "\n" +
                        "   - Battery\n" +
                        "   - Memory";
                buildAlertMessage(m);
            }
            else{
                if(checkG) {
                    m = " You have GREEN Light\n\n The mission is a GO : Rookie Mode\n" +
                            "\n" +
                            " Please check :\n" +
                            "\n" +
                            "   - Battery\n" +
                            "   - Memory";
                    buildAlertMessage(m);
                }
            }

        }
        }

        isFirstRun = false;

        //this.activity2.prefs.edit().putBoolean("isFirstRun", this.activity2.isFirstRun).commit();


    }
    }catch(Exception e){
    // Log.i("TestException",e.toString());

    }
    }




    public DataPoint[] RemoveNull(DataPoint[] dp) {


        List<DataPoint> list = new ArrayList<DataPoint>();

        for(DataPoint s : dp) {
            if(s != null ) {
                list.add(s);
            }
        }

        dp = list.toArray(new DataPoint[list.size()]);

        return dp;
    }

    boolean notNull(Object item) {
        return item != null;
    }


    void setimage(int min1, int max1, int min2, int max2, Double val, ImageButton img){
        if((val<min1)||(val>max1)){
            img.setImageResource(R.mipmap.cercle_rouge_icone_64);
            checkR=true;
        }
        else{
            if((val<min2)||(val>max2)){
                img.setImageResource(R.mipmap.cercle_orange_icone_64);
                checkO=true;
            }
            else{
                img.setImageResource(R.mipmap.cercle_vert_icone_64);
                checkG=true;
            }
        }

    }



    private void buildAlertMessage(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this.activity2.getActivity());
        builder.setMessage(msg)
                .setCancelable(false)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


}
