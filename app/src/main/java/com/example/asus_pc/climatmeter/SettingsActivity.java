package com.example.asus_pc.climatmeter;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by ASUS-PC on 28/05/2017.
 */

public class SettingsActivity extends PreferenceActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }

}
