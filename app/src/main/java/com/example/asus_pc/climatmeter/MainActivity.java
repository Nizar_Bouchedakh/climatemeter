package com.example.asus_pc.climatmeter;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements Fragment1.OnFragmentInteractionListener, Fragment2.OnFragmentInteractionListener, Fragment3.OnFragmentInteractionListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private ExamplePagerAdapter pagerAdapter;
    private ViewPager viewPager;
    private boolean mapReady = false;

    private Fragment1 F1;
    private Fragment2 F2;
    private  Fragment3 F3;
    private MapFragment FMap;

//    MyThread T1;
//    MyThread_img T2;
//    MyThread_GPS T3;

    int N1;
    int N2;
    int Freq1;
    int Freq2;
    int Freq3;
    boolean img;
    boolean gps;
    String IP;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPrefs.registerOnSharedPreferenceChangeListener(this);



        this.N1 = Integer.parseInt(sharedPrefs.getString("measurements_retrieval_window", "180"));
        this.N2 = Integer.parseInt(sharedPrefs.getString("gps_coord_retrieval_sampling_freq", "60"));

        this.Freq1 = Integer.parseInt(sharedPrefs.getString("measurements_retrieval_interval", "5"));
        this.Freq2 = Integer.parseInt(sharedPrefs.getString("image_retrieval_interval", "300"));
        this.Freq3 = Integer.parseInt(sharedPrefs.getString("gps_coord_retrieval_interval", "60"));

        this.img = sharedPrefs.getBoolean("image_retrieval", true);
        this.gps = sharedPrefs.getBoolean("gps_retrieval", true);

        this.IP = sharedPrefs.getString("IP", "192.168.2.1"); //10.0.0.5

        // location manager
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }





        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        pagerAdapter = new ExamplePagerAdapter(getSupportFragmentManager());
        F3 = Fragment3.newInstance(4,this.Freq1,this.IP);
        pagerAdapter.addFragment(F3, "Monitor");
        F1 = Fragment1.newInstance(this.N1,this.Freq1,this.IP);
        pagerAdapter.addFragment(F1, "Graph");
        F2 = Fragment2.newInstance(this.Freq2,this.IP,this.img);
        pagerAdapter.addFragment(F2, "Cam");
        FMap = MapFragment.newInstance(locationManager, this.N2, this.Freq3,this.IP, this.gps);
        pagerAdapter.addFragment(FMap, "Map");
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);

//        this.T1= new MyThread(F1, F3, N);
//        this.T1.start();
//        this.T2= new MyThread_img(F2);
////        this.T2.start();
//        this.T3= new MyThread_GPS(FMap, F);
//        this.T3.start();

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE );
        actionBar.setIcon(R.mipmap.backpack_100);
        actionBar.setTitle("   ClimatMeter       ");






        // Create a tab listener that is called when the user changes tabs.
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            }

            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // probably ignore this event
            }
        };

        for (int i = 0; i < pagerAdapter.getCount(); i++) {
            Fragment fragment = pagerAdapter.getItem(i);
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(pagerAdapter.getPageTitle(i))
                            .setTabListener(tabListener));
        }

        viewPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        // When swiping between pages, select the
                        // corresponding tab.
                        actionBar.setSelectedNavigationItem(position);
                    }
                });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_settings:
                Intent intent = new Intent(getApplicationContext(),
                        SettingsActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        return true;
    }


    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
// verifier la preference et agir !

    }

    @Override
    protected void onStart(){
        super.onStart();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1340);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onResume() {
        super.onResume();
////        this.T1.onResume();
//        this.T2.onResume();
//        this.T3.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
////        this.T1.onPause();
//        this.T2.onPause();
//        this.T3.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }



    @Override
    public void onFragmentInteraction(Uri uri) {

    }

//    @Override
//    public void onFragmentReady(MapFragment fragment) {
//        mapReady = true;
//        Log.i("MapsFragment" + fragment.hashCode(), "ready");
//        onFragmentsReady();
//    }

    private void onFragmentsReady() {

        if (!mapReady) return;
        Log.i("ResultsActivit" + hashCode(), "updating places...");
//        try {
//            updatePlaces();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


}
