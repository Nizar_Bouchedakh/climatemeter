package com.example.asus_pc.climatmeter;

/**
 * Created by ASUS-PC on 30/04/2017.
 */

public class MyThread_img extends Thread implements Runnable {

    private Object mPauseLock;
    private boolean mPaused;
    private boolean mFinished;

    Fragment2 activity;
    String IP = "192.168.2.1";
    int N;


    public MyThread_img(Fragment2 activity, int N, String IP){
        this.activity=activity;
        this.N=N;
        this.IP=IP;
        mPauseLock = new Object();
        mPaused = false;
        mFinished = false;
    }

    public void run() {
        while (!mFinished) {

            synchronized (mPauseLock) {
                try {
                    new Sender_Img(this.activity).execute("http://"+this.IP+":5001/getimage/cam1","http://"+this.IP+":5001/getimage/cam2");
                    //192.168.2.1 //10.0.0.5:5001
                    mPauseLock.wait(this.N*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                while (mPaused) {
                    try {
                        mPauseLock.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    /**
     * Call this on pause.
     */
    public void onPause() {
        synchronized (mPauseLock) {
            mPaused = true;
        }
    }

    /**
     * Call this on resume.
     */
    public void onResume() {
        synchronized (mPauseLock) {
            mPaused = false;
            mPauseLock.notifyAll();
        }
    }



}
