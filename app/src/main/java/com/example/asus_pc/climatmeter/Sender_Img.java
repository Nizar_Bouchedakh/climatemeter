package com.example.asus_pc.climatmeter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;


/**
 * Created by ASUS-PC on 28/04/2017.
 */

public class Sender_Img extends AsyncTask<String, Void, Bitmap[]> {

    private Fragment2 activity;

    Sender_Img(Fragment2 activity){
        super();
        this.activity = activity;
    }

    private Exception exception;

    protected Bitmap[] doInBackground(String... urls) {
        String r="OK";
        Bitmap bmp1 = null;
        Bitmap bmp2 = null;
        Bitmap[] bmp= new Bitmap[2];
        try {
            bmp1 = requestContent(urls[0]);
            // Log.i("Test", r);
            bmp[0]=bmp1;

            bmp2 = requestContent(urls[1]);
            // Log.i("Test", r);
            bmp[1]=bmp2;
        }
        catch(Exception e){
            r= e.toString();
            // Log.i("Test0",r);
        }
        return bmp;
    }

    // to send request
    public Bitmap requestContent(String url) {
        HttpClient httpclient = new DefaultHttpClient();
        String result = null;
        HttpGet httpget = new HttpGet(url);
        HttpResponse response = null;
        InputStream instream = null;
        Bitmap bmp=null;

        try {
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                instream = entity.getContent();
                bmp = BitmapFactory.decodeStream(instream);
            }

        } catch (Exception e) {
            // manage exceptions
            // Log.i("Test1", e.toString());
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (Exception exc) {
                    // Log.i("Test2", exc.toString());
                }
            }
        }

        return bmp;
    }






    protected void onPostExecute(Bitmap[] r) {
        // TODO: set imgview to img
        if(this.activity.img!=null) {
            this.activity.img.setImageBitmap(r[0]);
        }
        if(this.activity.img2!=null) {
            this.activity.img2.setImageBitmap(r[1]);
        }
    }



}
