package com.example.asus_pc.climatmeter;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by ASUS-PC on 20/05/2017.
 */

public class Fragment3 extends Fragment {


    MyThread_monitor T;
    int N ; // =4
    int F ;
    String IP;
    SharedPreferences prefs;

    public TextView StartTime;
    public TextView CurrentTime;
    public TextView Duration;
    
    public TextView GPS_Ready;
    public TextView Alt;
    public TextView NB_Sat;
    public TextView Flag;


    public TextView AWA;
    public TextView AWS;
    public TextView TWS;
    public TextView TWD;
    


    public TextView Temp;
    public TextView Noise;
    public TextView Hum;
    public TextView Solar;

    public TextView IMUY;
    public TextView IMUP;
    public TextView IMUR;

    public TextView TCB;
    public TextView TCM;
    public TextView TCT;

    public TextView IRBK;
    public TextView IRL;
    public TextView IRF;
    public TextView IRR;
    public TextView IRT;
    public TextView IRBT;

    public TextView SOG;
    public TextView COG;

    public ImageButton StartTimeImage;
    public ImageButton CurrentTimeImage;
    public ImageButton DurationImage;

    public ImageButton GPS_ReadyImage;
    public ImageButton AltImage;
    public ImageButton NB_SatImage;
    public ImageButton FlagImage;


    public ImageButton AWAImage;
    public ImageButton AWSImage;
    public ImageButton TWSImage;
    public ImageButton TWDImage;



    public ImageButton TempImage;
    public ImageButton NoiseImage;
    public ImageButton HumImage;
    public ImageButton SolarImage;

    public ImageButton IMUYImage;
    public ImageButton IMUPImage;
    public ImageButton IMURImage;

    public ImageButton TCBImage;
    public ImageButton TCMImage;
    public ImageButton TCTImage;

    public ImageButton IRBKImage;
    public ImageButton IRLImage;
    public ImageButton IRFImage;
    public ImageButton IRRImage;
    public ImageButton IRTImage;
    public ImageButton IRBTImage;

    public ImageButton SOGImage;
    public ImageButton COGImage;

    
    private Fragment3.OnFragmentInteractionListener mListener;


    public Fragment3() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment Fragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment3 newInstance(int N, int F, String IP) {
        Fragment3 fragment = new Fragment3();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.N=N;
        fragment.F=F;
        fragment.IP=IP;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        /*
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        */
        prefs = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        //isFirstRun = prefs.getBoolean("isFirstRun", true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_3, container, false);



        // Values
        this.StartTime = (TextView) view.findViewById(R.id.text11);
        this.CurrentTime = (TextView) view.findViewById(R.id.text12);
        this.Duration = (TextView) view.findViewById(R.id.text13);

        this.GPS_Ready = (TextView) view.findViewById(R.id.text21);
        this.Alt = (TextView) view.findViewById(R.id.text22);
        this.NB_Sat = (TextView) view.findViewById(R.id.text23);
        this.Flag = (TextView) view.findViewById(R.id.text24);

        this.AWA = (TextView) view.findViewById(R.id.text31);
        this.AWS = (TextView) view.findViewById(R.id.text32);
        this.TWS = (TextView) view.findViewById(R.id.text33);
        this.TWD = (TextView) view.findViewById(R.id.text34);


        this.Temp = (TextView) view.findViewById(R.id.text41);
        this.Noise = (TextView) view.findViewById(R.id.text42);
        this.Hum = (TextView) view.findViewById(R.id.text43);
        this.Solar = (TextView) view.findViewById(R.id.text44);

        this.IMUY = (TextView) view.findViewById(R.id.text51);
        this.IMUP = (TextView) view.findViewById(R.id.text52);
        this.IMUR = (TextView) view.findViewById(R.id.text53);

        this.TCB = (TextView) view.findViewById(R.id.text61);
        this.TCM = (TextView) view.findViewById(R.id.text62);
        this.TCT = (TextView) view.findViewById(R.id.text63);

        this.IRBK = (TextView) view.findViewById(R.id.text71);
        this.IRL = (TextView) view.findViewById(R.id.text72);
        this.IRF = (TextView) view.findViewById(R.id.text73);
        this.IRR = (TextView) view.findViewById(R.id.text74);
        this.IRT = (TextView) view.findViewById(R.id.text81);
        this.IRBT = (TextView) view.findViewById(R.id.text82);

        this.SOG = (TextView) view.findViewById(R.id.text83);
        this.COG = (TextView) view.findViewById(R.id.text84);


// images
        this.StartTimeImage = (ImageButton) view.findViewById(R.id.button11);
        this.CurrentTimeImage = (ImageButton) view.findViewById(R.id.button12);
        this.DurationImage = (ImageButton) view.findViewById(R.id.button13);

        this.GPS_ReadyImage = (ImageButton) view.findViewById(R.id.button21);
        this.AltImage = (ImageButton) view.findViewById(R.id.button22);
        this.NB_SatImage = (ImageButton) view.findViewById(R.id.button23);
        this.FlagImage = (ImageButton) view.findViewById(R.id.button24);

        this.AWAImage = (ImageButton) view.findViewById(R.id.button31);
        this.AWSImage = (ImageButton) view.findViewById(R.id.button32);
        this.TWSImage = (ImageButton) view.findViewById(R.id.button33);
        this.TWDImage = (ImageButton) view.findViewById(R.id.button34);


        this.TempImage = (ImageButton) view.findViewById(R.id.button41);
        this.NoiseImage = (ImageButton) view.findViewById(R.id.button42);
        this.HumImage = (ImageButton) view.findViewById(R.id.button43);
        this.SolarImage = (ImageButton) view.findViewById(R.id.button44);

        this.IMUYImage = (ImageButton) view.findViewById(R.id.button51);
        this.IMUPImage = (ImageButton) view.findViewById(R.id.button52);
        this.IMURImage = (ImageButton) view.findViewById(R.id.button53);

        this.TCBImage = (ImageButton) view.findViewById(R.id.button61);
        this.TCMImage = (ImageButton) view.findViewById(R.id.button62);
        this.TCTImage = (ImageButton) view.findViewById(R.id.button63);

        this.IRBKImage = (ImageButton) view.findViewById(R.id.button71);
        this.IRLImage = (ImageButton) view.findViewById(R.id.button72);
        this.IRFImage = (ImageButton) view.findViewById(R.id.button73);
        this.IRRImage = (ImageButton) view.findViewById(R.id.button74);
        this.IRTImage = (ImageButton) view.findViewById(R.id.button81);
        this.IRBTImage = (ImageButton) view.findViewById(R.id.button82);

        this.SOGImage = (ImageButton) view.findViewById(R.id.button83);
        this.COGImage = (ImageButton) view.findViewById(R.id.button84);





        this.StartTime.setText("Start Time");
        this.CurrentTime.setText("Current Time");
        this.Duration.setText("Duration");

        this.GPS_Ready.setText("GPS");
        this.Alt.setText("ALT");
        this.NB_Sat.setText("Sat_Number");
        this.Flag.setText("Flag");

        this.AWA.setText("AWA");
        this.AWS.setText("AWS");
        this.TWS.setText("TWS");
        this.TWD.setText("TWD");


        this.Temp.setText("Temperature");
        this.Noise.setText("Noise");
        this.Hum.setText("Humidity");
        this.Solar.setText("Solar");

        this.IMUY.setText("IMU_Y");
        this.IMUP.setText("IMU_P");
        this.IMUR.setText("IMU_R");

        this.TCB.setText("TCB");
        this.TCM.setText("TCM");
        this.TCT.setText("TCT");

        this.IRBK.setText("IRBK");
        this.IRL.setText("IRL");
        this.IRF.setText("IRF");
        this.IRR.setText("IRR");
        this.IRT.setText("IRT");
        this.IRBT.setText("IRBT");

        this.SOG.setText("SOG");
        this.COG.setText("COG");




        this.StartTimeImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.CurrentTimeImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.DurationImage.setImageResource(R.mipmap.cercle_rouge_icone_64);

        this.GPS_ReadyImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.AltImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.NB_SatImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.FlagImage.setImageResource(R.mipmap.cercle_rouge_icone_64);

        this.AWAImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.AWSImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.TWSImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.TWDImage.setImageResource(R.mipmap.cercle_rouge_icone_64);


        this.TempImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.NoiseImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.HumImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.SolarImage.setImageResource(R.mipmap.cercle_rouge_icone_64);

        this.IMUYImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.IMUPImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.IMURImage.setImageResource(R.mipmap.cercle_rouge_icone_64);

        this.TCBImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.TCMImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.TCTImage.setImageResource(R.mipmap.cercle_rouge_icone_64);

        this.IRBKImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.IRLImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.IRFImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.IRRImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.IRTImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.IRBTImage.setImageResource(R.mipmap.cercle_rouge_icone_64);

        this.SOGImage.setImageResource(R.mipmap.cercle_rouge_icone_64);
        this.COGImage.setImageResource(R.mipmap.cercle_rouge_icone_64);




        this.T= new MyThread_monitor(this, this.N, this.F, this.IP);
        this.T.start();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        this.T.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        this.T.onPause();
    }





    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Fragment1.OnFragmentInteractionListener) {
            mListener = (Fragment3.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

        public interface OnFragmentInteractionListener {
            // TODO: Update argument type and name
            void onFragmentInteraction(Uri uri);
        }

}


