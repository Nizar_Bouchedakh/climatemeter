package com.example.asus_pc.climatmeter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Fragment1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Fragment1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment1 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    // TODO: Rename and change types of parameters


    public TextView Alt;
    public TextView NB_Sat;
    public TextView AWA;
    public TextView AWS;
    public TextView TWS;
    public TextView TWD;
    public TextView Flag;
    public TextView SOG;
    public TextView COG;
    public TextView StartTime;
    public TextView CurrentTime;
    public TextView Duration;
    public RadioButton GPS_Ready;


    public GraphView graph1;
    public GraphView graph2;
    public GraphView graph3;

    MyThread T;
    Fragment3 F3;
    LineGraphSeries<DataPoint> Tempseries;
    LineGraphSeries<DataPoint> Noiseseries;
    LineGraphSeries<DataPoint> Humseries;
    LineGraphSeries<DataPoint> Solarseries;
    LineGraphSeries<DataPoint> IMUYseries;
    LineGraphSeries<DataPoint> IMUPseries;
    LineGraphSeries<DataPoint> IMURseries;
    LineGraphSeries<DataPoint> TCBseries;
    LineGraphSeries<DataPoint> TCMseries;
    LineGraphSeries<DataPoint> TCTseries;
    LineGraphSeries<DataPoint> IRBKseries;
    LineGraphSeries<DataPoint> IRLseries;
    LineGraphSeries<DataPoint> IRFseries;
    LineGraphSeries<DataPoint> IRRseries;
    LineGraphSeries<DataPoint> IRTseries;
    LineGraphSeries<DataPoint> IRBTseries;
    int N ; //= 180;
    int F;
    String IP;

    private OnFragmentInteractionListener mListener;

    public Fragment1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment Fragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment1 newInstance(int N, int F, String IP) {
        Fragment1 fragment = new Fragment1();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.N=N;
        fragment.F=F;
        fragment.IP=IP;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        /*
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        */
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_1, container, false);

        // Values
        this.StartTime = (TextView) view.findViewById(R.id.textView12);
        this.CurrentTime = (TextView) view.findViewById(R.id.textView11);
        this.Duration = (TextView) view.findViewById(R.id.textView10);
        this.Alt = (TextView) view.findViewById(R.id.textView9);
        this.NB_Sat = (TextView) view.findViewById(R.id.textView8);
        this.Flag = (TextView) view.findViewById(R.id.textView7);
        this.AWA = (TextView) view.findViewById(R.id.textView6);
        this.AWS = (TextView) view.findViewById(R.id.textView5);
        this.TWS = (TextView) view.findViewById(R.id.textView4);
        this.TWD = (TextView) view.findViewById(R.id.textView3);
        this.SOG = (TextView) view.findViewById(R.id.textView2);
        this.COG = (TextView) view.findViewById(R.id.textView1);
        this.GPS_Ready = (RadioButton) view.findViewById(R.id.radioButton);


        this.StartTime.setText("Start Time");
        this.CurrentTime.setText("Current Time");
        this.Duration.setText("Duration");
        this.Alt.setText("ALT");
        this.NB_Sat.setText("Sat_Number");
        this.AWA.setText("AWA");
        this.AWS.setText("AWS");
        this.TWS.setText("TWS");
        this.TWD.setText("TWD");
        this.Flag.setText("Flag");
        this.SOG.setText("SOG");
        this.COG.setText("COG");
        this.GPS_Ready.setChecked(false);




        // Graph1
                    this.graph1 = (GraphView) view.findViewById(R.id.graph1);
            
                    // initialize graph and series
            
                    DataPoint[] data = new DataPoint[0]; //initialize as empty
                    Tempseries = new LineGraphSeries<>(data);
                    Noiseseries = new LineGraphSeries<>(data);
                    Humseries = new LineGraphSeries<>(data);
                    Solarseries = new LineGraphSeries<>(data);
            /*
            
                    // set listener for data inspection ( one listener per serie)
            
                    Tempseries.setOnDataPointTapListener(new OnDataPointTapListener() {
                        @Override
                        public void onTap(Series series, DataPointInterface dataPoint) {
                            Toast.makeText(getActivity(), "Temperature: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                        }
                    });
            
                    Noiseseries.setOnDataPointTapListener(new OnDataPointTapListener() {
                        @Override
                        public void onTap(Series series, DataPointInterface dataPoint) {
                            Toast.makeText(getActivity(), "Noise Level: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                        }
                    });
            
                    Solarseries.setOnDataPointTapListener(new OnDataPointTapListener() {
                        @Override
                        public void onTap(Series series, DataPointInterface dataPoint) {
                            Toast.makeText(getActivity(), "Solar Gh: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                        }
                    });

                    Humseries.setOnDataPointTapListener(new OnDataPointTapListener() {
                        @Override
                        public void onTap(Series series, DataPointInterface dataPoint) {
                            Toast.makeText(getActivity(), "Relative Humidity: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                        }
                    });
            */
            
                    // set date label formatter (date as X, numbers as Y)
            
                    this.graph1.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getContext(),new SimpleDateFormat("HH:mm:ss")));
                    this.graph1.getGridLabelRenderer().setHorizontalLabelsAngle(10);
                    this.graph1.getGridLabelRenderer().setLabelVerticalWidth(70);
                    //this.graph1.getGridLabelRenderer().setVerticalLabelsAlign(Paint.Align.CENTER);
                    //this.graph1.getGridLabelRenderer().setSecondScaleLabelVerticalWidth(200);
                    this.graph1.getGridLabelRenderer().setPadding(30);
                    this.graph1.getGridLabelRenderer().setNumHorizontalLabels(6);
                    this.graph1.getGridLabelRenderer().setHighlightZeroLines(true);
                    this.graph1.getViewport().setScalable(true);
                    this.graph1.getViewport().setScrollable(false);
                    this.graph1.getGridLabelRenderer().setNumVerticalLabels(11);
                    this.graph1.getGridLabelRenderer().setHorizontalAxisTitle("Time (UTC)");
                    this.graph1.getGridLabelRenderer().setGridColor(Color.GRAY);

                    // the y bounds are always manual for second scale
                    this.graph1.getSecondScale().setMinY(0);
                    this.graph1.getSecondScale().setMaxY(140);
            
            
                    // set second scale (noise for now)
            
                    this.graph1.getGridLabelRenderer().setVerticalLabelsSecondScaleColor(Color.BLUE);
                    this.graph1.getGridLabelRenderer().setVerticalLabelsSecondScaleAlign(Paint.Align.LEFT);
            
            
                    // set graphs colors
            
                    Tempseries.setColor(Color.RED);
                    Humseries.setColor(Color.GREEN);
                    Noiseseries.setColor(Color.BLUE);
                    Solarseries.setColor(Color.YELLOW);

                    // legend
                    Tempseries.setTitle("Temp");
                    Humseries.setTitle("Hum");
                    Noiseseries.setTitle("Noise");
                    Solarseries.setTitle("Solar");
                    this.graph1.getLegendRenderer().setVisible(true);
                    this.graph1.getLegendRenderer().setBackgroundColor(Color.GRAY);
                    this.graph1.getLegendRenderer().setFixedPosition(0,0);


        // Pass series
            
                    this.graph1.addSeries(Tempseries);
                    this.graph1.addSeries(Humseries);
                    this.graph1.addSeries(Solarseries);
                    this.graph1.getSecondScale().addSeries(Noiseseries);







            // Graph2
            this.graph2 = (GraphView) view.findViewById(R.id.graph2);
    
            // initialize graph and series
    
    
            TCBseries = new LineGraphSeries<>(data);
            TCMseries = new LineGraphSeries<>(data);
            TCTseries = new LineGraphSeries<>(data);

            IMUYseries = new LineGraphSeries<>(data);
            IMUPseries = new LineGraphSeries<>(data);
            IMURseries = new LineGraphSeries<>(data);
    
    /*
            // set listener for data inspection ( one listener per serie)
    
            TCBseries.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getActivity(), "TCB: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });
    
            TCMseries.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getActivity(), "TCM: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });
    
            TCTseries.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getActivity(), "TCT: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });

            IMUYseries.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getActivity(), "IMU_Y: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });

            IMUPseries.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getActivity(), "IMU_P: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });

            IMURseries.setOnDataPointTapListener(new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    Toast.makeText(getActivity(), "IMU_R: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                }
            });
    
    */
            // set date label formatter (date as X, numbers as Y)
    
            this.graph2.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getContext(),new SimpleDateFormat("HH:mm:ss")));
            this.graph2.getGridLabelRenderer().setHorizontalLabelsAngle(10);
            this.graph2.getGridLabelRenderer().setLabelVerticalWidth(50);
            //this.graph2.getGridLabelRenderer().setVerticalLabelsAlign(Paint.Align.CENTER);
            //this.graph2.getGridLabelRenderer().setSecondScaleLabelVerticalWidth(200);
            //this.graph2.getGridLabelRenderer().setPadding(10);
            this.graph2.getGridLabelRenderer().setNumHorizontalLabels(6);
            this.graph2.getGridLabelRenderer().setHighlightZeroLines(true);
            this.graph2.getViewport().setScalable(true);
            this.graph2.getViewport().setScrollable(false);
            this.graph2.getGridLabelRenderer().setNumVerticalLabels(11);
            this.graph2.getGridLabelRenderer().setHorizontalAxisTitle("Time (UTC)");
            //this.graph2.getGridLabelRenderer().setVerticalAxisTitle("Thermo couples");
            this.graph2.getGridLabelRenderer().setGridColor(Color.GRAY);

            this.graph2.getGridLabelRenderer().setVerticalLabelsSecondScaleColor(Color.MAGENTA);
            this.graph2.getGridLabelRenderer().setVerticalLabelsSecondScaleAlign(Paint.Align.LEFT);

            this.graph2.getSecondScale().setMinY(-180);
            this.graph2.getSecondScale().setMaxY(360);
    
    
    
    
            // set graphs colors
    
            TCBseries.setColor(Color.RED);
            TCMseries.setColor(Color.GREEN);
            TCTseries.setColor(Color.BLUE);

            IMUYseries.setColor(Color.MAGENTA);
            IMUPseries.setColor(Color.YELLOW);
            IMURseries.setColor(Color.CYAN);
            // legend
            TCBseries.setTitle("TC_Bottom");
            TCMseries.setTitle("TC_Middle");
            TCTseries.setTitle("TC_Top");
            IMUYseries.setTitle("IMU_Y");
            IMUPseries.setTitle("IMU_P");
            IMURseries.setTitle("IMU_R");
            this.graph2.getLegendRenderer().setVisible(true);
            this.graph2.getLegendRenderer().setBackgroundColor(Color.GRAY);
            this.graph2.getLegendRenderer().setFixedPosition(0,0);
    
            // Pass series
    
            this.graph2.addSeries(TCBseries);
            this.graph2.addSeries(TCMseries);
            this.graph2.addSeries(TCTseries);

            this.graph2.getSecondScale().addSeries(IMUYseries);
            this.graph2.getSecondScale().addSeries(IMUPseries);
            this.graph2.getSecondScale().addSeries(IMURseries);





        // Graph3
        this.graph3 = (GraphView) view.findViewById(R.id.graph3);

        // initialize graph and series


        IRBKseries = new LineGraphSeries<>(data);
        IRLseries = new LineGraphSeries<>(data);
        IRFseries = new LineGraphSeries<>(data);
        IRRseries = new LineGraphSeries<>(data);
        IRTseries = new LineGraphSeries<>(data);
        IRBTseries = new LineGraphSeries<>(data);

/*
        // set listener for data inspection ( one listener per serie)

        IRBKseries.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(getActivity(), "TC Back: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
            }
        });

        IRLseries.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(getActivity(), "TC Left: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
            }
        });

        IRFseries.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(getActivity(), "TC Front: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
            }
        });

        IRRseries.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(getActivity(), "TC Right: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
            }
        });

        IRTseries.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(getActivity(), "TC Top: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
            }
        });

        IRBTseries.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(getActivity(), "TC Bottom: "+getDate(dataPoint.getX())+" - "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
            }
        });
*/
        // set date label formatter (date as X, numbers as Y)

        this.graph3.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getContext(),new SimpleDateFormat("HH:mm:ss")));
        this.graph3.getGridLabelRenderer().setHorizontalLabelsAngle(10);
        this.graph3.getGridLabelRenderer().setLabelVerticalWidth(70);
        //this.graph3.getGridLabelRenderer().setVerticalLabelsAlign(Paint.Align.CENTER);
        //this.graph3.getGridLabelRenderer().setSecondScaleLabelVerticalWidth(200);
        this.graph3.getGridLabelRenderer().setPadding(10);
        this.graph3.getGridLabelRenderer().setNumHorizontalLabels(6);
        this.graph3.getGridLabelRenderer().setHighlightZeroLines(true);
        this.graph3.getViewport().setScalable(true);
        this.graph3.getViewport().setScrollable(false);
        this.graph3.getGridLabelRenderer().setNumVerticalLabels(11);
        this.graph3.getGridLabelRenderer().setHorizontalAxisTitle("Time (UTC)");
        //this.graph3.getGridLabelRenderer().setVerticalAxisTitle("INFRARED");
        this.graph3.getGridLabelRenderer().setGridColor(Color.GRAY);




        // set graphs colors

        IRBKseries.setColor(Color.RED);
        IRLseries.setColor(Color.GREEN);
        IRFseries.setColor(Color.BLUE);
        IRRseries.setColor(Color.CYAN);
        IRTseries.setColor(Color.YELLOW);
        IRBTseries.setColor(Color.MAGENTA);

        // legend
        IRBKseries.setTitle("IR_Back");
        IRLseries.setTitle("IR_Left");
        IRFseries.setTitle("IR_Front");
        IRRseries.setTitle("IR_Right");
        IRTseries.setTitle("IR_Top");
        IRBTseries.setTitle("IR_Bottom");
        this.graph3.getLegendRenderer().setVisible(true);
        this.graph3.getLegendRenderer().setBackgroundColor(Color.GRAY);
        this.graph3.getLegendRenderer().setFixedPosition(0,0);

        // Pass series

        this.graph3.addSeries(IRBKseries);
        this.graph3.addSeries(IRLseries);
        this.graph3.addSeries(IRFseries);
        this.graph3.addSeries(IRRseries);
        this.graph3.addSeries(IRTseries);
        this.graph3.addSeries(IRBTseries);
        
        
        // Start Thread for periodic data acquisition
        this.T= new MyThread(this, this.N , this.F, this.IP);
        this.T.start();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        this.T.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        this.T.onPause();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }




    private String getDate(double timeStamp){

        try{
            DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy-HH:mm:ss");
            Date netDate = (new Date((long) timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
