package com.example.asus_pc.climatmeter;

import android.graphics.Color;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.jjoe64.graphview.series.DataPoint;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by ASUS-PC on 28/04/2017.
 */

public class Sender_GPS extends AsyncTask<String, Void, String> {

    private MapFragment activity;

    Sender_GPS(MapFragment activity){
        super();
        this.activity = activity;
    }

    private Exception exception;

    protected String doInBackground(String... urls) {
        String r;
        try {
            r = requestContent(urls[0]);
            // Log.i("TestGPS", r);
        }
        catch(Exception e){
            r= e.toString();
            // Log.i("TestGPS",r);
        }
        return r;
    }

    // to send request
    public String requestContent(String url) {
        HttpClient httpclient = new DefaultHttpClient();
        String result = null;
        HttpGet httpget = new HttpGet(url);
        HttpResponse response = null;
        InputStream instream = null;

        try {
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                instream = entity.getContent();
                result = convertStreamToString(instream);
            }

        } catch (Exception e) {
            // manage exceptions
            // Log.i("Test1", e.toString());
        } finally {
            if (instream != null) {
                try {
                    instream.close();
                } catch (Exception exc) {
                    // Log.i("Test2", exc.toString());
                }
            }
        }

        return result;
    }

    // convert data stream into string
    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            // Log.i("Test3", e.toString());
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                // Log.i("Test4", e.toString());
            }
        }

        return sb.toString();
    }

    protected void onPostExecute(String r) {
        // TODO: check this.exception
        // TODO: get points and plot them on the map
        // TODO: print starttime (first GPS)

    try{

        // Log.i("TestGPS1", r);


        String[] measures = r.split(";");
        // Log.i("TestGPS2",measures[0]);


        String[] First_serie = measures[0].split(",");
        String First_time = First_serie[0].replace("*#", "").replace("[", "").replace("'", "").replace("\"", "");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        Date First_date = null;

        try {
            First_date = sdf.parse(First_time);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        Double First_Lng = Double.parseDouble(First_serie[1].replace("'", ""));
        Double First_Lat= Double.parseDouble(First_serie[2].replace("'", "").replace("]", ""));




        for(int j=1;j<measures.length-1;j++){

            String[] serie = measures[j].split(",");
            String time = serie[0].replace("*#", "").replace("[", "").replace("'", "").replace("\"", "");

            Date date = null;
            try {
                date = sdf.parse(time);

            } catch (ParseException e) {
                e.printStackTrace();
            }


            Double Lng = Double.parseDouble(serie[1].replace("'", ""));
            Double Lat= Double.parseDouble(serie[2].replace("'", "").replace("]", ""));

            // Log.i("TestGPS3", time+Lng+Lat);


                Polyline line = this.activity.mMap.addPolyline(new PolylineOptions()
                        .add(new LatLng(First_Lat, First_Lng), new LatLng(Lat, Lng))
                        .width(5)
                        .color(Color.BLUE));
            this.activity.mMap.addMarker(new MarkerOptions().position(new LatLng(Lat, Lng)).title(date.toString()).icon(BitmapDescriptorFactory.defaultMarker((float)j*(360/measures.length))));

            First_Lat = Lat;
            First_Lng = Lng;
            First_date = date;

        }
        //this.activity.zooming(First_Lat,First_Lng);

 /*
// code
        PolylineOptions rectOptions = new PolylineOptions();
                        //this is the color of route
                        rectOptions.color(Color.argb(255, 85, 166, 27));

                        LatLng startLatLng = null;
                        LatLng endLatLng = null;
                        for (int i = 0; i < newLatt.size(); i++) {
                        Double lati = Double.parseDouble(newLatt.get(i));
                        Double longi = Double.parseDouble(newLongg.get(i));
                            LatLng latlng = new LatLng(lati,
                                    longi);
                            if (i == 0) {
                                startLatLng = latlng;
                            }
                            if (i == jArr.length() - 1) {
                                endLatLng = latlng;
                            }
                            rectOptions.add(latlng);
                        }
                        map.addPolyline(rectOptions);


// code 2
  GoogleMap map;
   // ... get a map.
   // Add a thin red line from London to New York.

        //TODO: Use appenddata and keep viewport on current zoom position (current MinX & MaxX)
 */
        }catch(Exception e){
            // Log.i("TestGPSException",e.toString());
        }
    }




    public DataPoint[] RemoveNull(DataPoint[] dp) {


        List<DataPoint> list = new ArrayList<DataPoint>();

        for(DataPoint s : dp) {
            if(s != null ) {
                list.add(s);
            }
        }

        dp = list.toArray(new DataPoint[list.size()]);

        return dp;
    }

    boolean notNull(Object item) {
        return item != null;
    }




}
